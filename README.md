HubWub
======

Have you ever needed a plugin where you needed everyone invisible in your hub except for your Staff Members to reduce lag? I did. And that is why I created HubWub. HubWub makes it so all players are invisible on your hub except for the players with; hubwub.visible !

 **Simple Description**: Make Players Invisible On Your Hub. Players with hubwub.visible will be seen to all players.

 **Current Version**: 1.0 -- Pizza Out Of The Oven!
 * **Commands**: None
 * **Permission Nodes**: hubwub.visible - Makes You Visible To All Players!
