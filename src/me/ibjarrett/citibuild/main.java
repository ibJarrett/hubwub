package me.ibjarrett.citibuild;


import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class main extends JavaPlugin implements Listener{
	static final String name = "HubWub";
	static final String perm = "hubwub.visible"; 

	@Override public void onEnable(){
		getLogger().info(name+" has been enabled");
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(this, this);
		this.getCommand("hubreload").setExecutor(this);
	}

	@Override public void onDisable(){
		getLogger().info(name+" has been disabled");
	}

	@EventHandler public void onPlayerJoin(PlayerJoinEvent event){
		event.setJoinMessage(null);
		runPlayer(event.getPlayer());
	}

	@EventHandler public void onPlayerQuit(PlayerQuitEvent event){
		event.setQuitMessage(null);
	}

	public void update(){
		for (Player player : getServer().getOnlinePlayers()){
			runPlayer(player);
		}
	}

	public void cleanUp(){
		for (Player player1 : getServer().getOnlinePlayers()){
			for (Player player2 : getServer().getOnlinePlayers()){
				player1.showPlayer(player2);
			}
		}
	}

	private void runPlayer(Player play){
		if(!play.hasPermission(perm)){
			for(Player p: getServer().getOnlinePlayers()){
			}
		}
		for (Player player : getServer().getOnlinePlayers()) {
			if(!play.hasPermission(perm))player.hidePlayer(play);
			if(!player.hasPermission(perm))play.hidePlayer(player);
		}
	}
}